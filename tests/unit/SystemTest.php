<?php
use PHPUnit\Framework\TestCase;

class SystemTest extends TestCase
{
    public function test_system_notify_existing_username(){
        global $user;
        return $this->assertEquals("This username already taken" ,$user-> checkuser("nahom"));
    }

    public function test_system_notify_avilable_username(){
        global $user;
        return $this->assertEquals("username available" ,$user-> checkuser("nah"));
    }

}