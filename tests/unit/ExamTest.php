<?php
/**
 * Created by PhpStorm.
 * User: abenezerbekele
 * Date: 7/10/18
 * Time: 11:41 AM
 */
use PHPUnit\Framework\TestCase;

class ExamTest extends TestCase
{
    public function test_user_can_take_exam(){
    global $exam;
    return $this->assertEquals(true,$exam->TUpdateProgress(85,2,1,8) );
}

    public function  test_user_can_update_pregress(){
        global $exam;
        return $this->assertEquals(true,$exam->TUpdateProgress2(85,1,8) );
    }

    public function test_user_can_show_progress()
    {
        global $exam;
        return $this->assertEquals(false , $exam->getProgress(85,6));
    }
}