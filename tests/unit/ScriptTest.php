<?php
/**
 * Created by PhpStorm.
 * User: abenezerbekele
 * Date: 7/9/18
 * Time: 8:34 PM
 */

use PHPUnit\Framework\TestCase;

class ScriptTest extends TestCase
{

    public function test_user_can_write_on_afile()
    {
        global $scriptHandler;
        return $this->assertEquals(true, $scriptHandler->TWriteToFile("<h1>hello</h1>"));
    }

    public function test_system_returns_interpretable_code_with_iframe()
    {
        global $scriptHandler;
        $scriptHandler->TWriteToFile("<h1>hello</h1>");
        return $this->assertEquals(" <iframe class='embed-responsive-item' src='script.php'></iframe>", $scriptHandler->getResponse());
    }

    public function test_user_can_write_and_interpret_html_code()
    {
        global $scriptHandler;
        $scriptHandler->TWriteToFile("<h1>hello</h1>");

        return $this->assertEquals(" <iframe class='embed-responsive-item' src='script.php'></iframe>", $scriptHandler->getResponse());
    }

    public function test_user_can_write_and_interpret_php_code()
    {
        global $scriptHandler;
        $scriptHandler->TWriteToFile("<h1>hello</h1>");

        return $this->assertEquals(" <iframe class='embed-responsive-item' src='script.php'></iframe>", $scriptHandler->getResponse());
    }

    public function test_user_can_write_and_interpret_css_code()
    {
        global $scriptHandler;
        $scriptHandler->TWriteToFile("<h1>hello</h1>");

        return $this->assertEquals(" <iframe class='embed-responsive-item' src='script.php'></iframe>", $scriptHandler->getResponse());
    }

    public function test_user_can_write_and_interpret_javascript_code()
    {
        global $scriptHandler;
        $scriptHandler->TWriteToFile("<h1>hello</h1>");

        return $this->assertEquals(" <iframe class='embed-responsive-item' src='script.php'></iframe>", $scriptHandler->getResponse());
    }

    public function test_user_can_write_and_compile_python_code()
    {
        global $scriptHandler;
        $scriptHandler->TWriteToFile("<h1>hello</h1>");

        return $this->assertEquals(" <iframe class='embed-responsive-item' src='script.php'></iframe>", $scriptHandler->getResponse());
    }


}