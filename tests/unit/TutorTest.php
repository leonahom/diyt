<?php
/**
 * Created by PhpStorm.
 * User: abenezerbekele
 * Date: 7/9/18
 * Time: 7:14 PM
 */
use PHPUnit\Framework\TestCase;

class TutorTest extends TestCase
{
    public function test_system_returns_chapters(){
        global $tutors;
        return $this->assertEquals("" ,$tutors-> getCh(1));
    }

    public function test_system__show_all_chapters(){
        global $tutors;
        return $this->assertEquals("<button class='bt' href='#' name='chid' value='2' type='submit'>HTML</button>" ,$tutors-> getChapters(1));
    }


}