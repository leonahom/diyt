<?php
/**
 * Created by PhpStorm.
 * User: abenezerbekele
 * Date: 7/10/18
 * Time: 4:05 PM
 */
use PHPUnit\Framework\TestCase;

class ForumTest extends TestCase
{
    public function test_user_can_create_topic(){
        global $forum;
        global $forumexpect;
        return $this->assertEquals($forumexpect ,$forum -> TCreatetopic() );
    }
    public function test_user_can_participate_on_forum(){
        global $forum;
        global $forumexpect;
        return $this->assertEquals($forumexpect  ,$forum -> MakeComment() );
    }
}