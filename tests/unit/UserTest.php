<?php
/**
 * Created by PhpStorm.
 * User: abenezerbekele
 * Date: 7/9/18
 * Time: 1:41 PM
 */
use PHPUnit\Framework\TestCase;
require "app/models/user_model.php";
require "app/controllers/tutors_controller.php";
require "app/controllers/handleScript.php";
require "app/script.php";
require "app/functions.php";
require "app/models/testmodel.php";
require "app/models/forummodel.php";

class UserTest extends TestCase
{

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function test_user_can_login(){
        global $user;
        return $this->assertEquals(true ,$user-> TLogin("nahom","nahom123"));
    }

    public function test_user_cannot_login_with_wrong_cridentials(){
        global $user;
        return $this->assertEquals(false ,$user-> TLogin("naho2","nahom123"));
    }

    public function test_user_cannot_login_with_password_length_less_than_8(){
        global $user;
        return $this->assertEquals(false ,$user-> TLogin("naho2","nahom"));
    }

    public function test_user_can_register(){
        global $user;
        return $this->assertEquals(true,$user->TRegisterUser($this->generateRandomString(),$this->generateRandomString(),$this->generateRandomString()."@gmail.com"
        ,$this->generateRandomString(),"123123123","123123123"));
    }

    public function test_user_cannot_edit_profile_with_out_existing_data(){
        global $user;
        return $this->assertEquals(false,$user->editpro());
    }




}