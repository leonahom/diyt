-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2017 at 04:13 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `forumpractice`
--

-- --------------------------------------------------------

--
-- Table structure for table `catagories`
--

CREATE TABLE `catagories` (
  `id` tinyint(4) NOT NULL,
  `catagory_title` varchar(150) NOT NULL,
  `catagory_discription` varchar(255) NOT NULL,
  `last_post_date` datetime DEFAULT NULL,
  `last_user_posted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `catagories`
--

INSERT INTO `catagories` (`id`, `catagory_title`, `catagory_discription`, `last_post_date`, `last_user_posted`) VALUES
(1, 'HTML and CSS forum ', 'This is the first test category', '2017-07-27 02:32:18', 0),
(2, 'General forum', 'This is a general forum category', '2017-07-27 14:32:57', 5),
(3, 'java script forum', 'This is the java script catagory', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `chid` int(11) NOT NULL,
  `title` varchar(20) DEFAULT NULL,
  `contente` varchar(2000) DEFAULT NULL,
  `contenta` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `courseid` int(11) DEFAULT NULL,
  `titlea` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`chid`, `title`, `contente`, `contenta`, `courseid`, `titlea`) VALUES
(2, 'HTML', 'dvnsf;nvb''sdnbdsnbv', 'dsvsdnvb''sfnbl', 1, 'asfafad');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `courseid` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`courseid`, `name`) VALUES
(1, 'HTML'),
(2, 'CSS'),
(3, 'Java Script'),
(4, 'PHP'),
(5, 'Python');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `optid` int(11) NOT NULL,
  `qid` int(11) DEFAULT NULL,
  `answer` tinyint(1) DEFAULT NULL,
  `contente` varchar(100) DEFAULT NULL,
  `contenta` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`optid`, `qid`, `answer`, `contente`, `contenta`) VALUES
(1, 1, 1, 'True', ''),
(2, 1, 0, 'False', ''),
(3, 2, 0, 'True', ''),
(4, 2, 1, 'False', ''),
(5, 3, 0, 'True', ''),
(6, 3, 1, 'False', ''),
(7, 4, 1, 'True', ''),
(8, 4, 0, 'False', '');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `catagory_id` tinyint(4) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `post_creator` int(11) NOT NULL,
  `post_content` text NOT NULL,
  `post_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `catagory_id`, `topic_id`, `post_creator`, `post_content`, `post_date`) VALUES
(9, 2, 11, 0, 'Test Content', '2017-07-27 01:07:18'),
(10, 2, 11, 0, 'This is the reply content', '2017-07-27 02:26:18'),
(11, 1, 12, 0, 'Test Content 1', '2017-07-27 02:27:35'),
(12, 1, 12, 0, 'Test reply for test content 1', '2017-07-27 02:27:52'),
(13, 1, 12, 0, 'something', '2017-07-27 02:29:39'),
(14, 1, 12, 0, 'something something', '2017-07-27 02:32:18'),
(15, 2, 11, 5, '123456', '2017-07-27 14:32:57');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `qid` int(11) NOT NULL,
  `courseid` int(11) DEFAULT NULL,
  `questione` varchar(100) DEFAULT NULL,
  `questiona` varchar(100) DEFAULT NULL,
  `chid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`qid`, `courseid`, `questione`, `questiona`, `chid`) VALUES
(1, 1, '1.The ''p'' tag in HTML helps to write any message you want to deliver', 'The ''p'' tag in HTML helps to write any message you want to deliver', 2),
(2, 1, '2.The ''h1'' tag in HTML sends an output the same as the ''p'' tag', NULL, 2),
(3, 1, '3.End tags are optional or you may not close any tag you opened before', '3.End tags are optional or you may not close any tag you opened before', 2),
(4, 1, '4.The ''table'' tag helps to build a table', '4.The ''table'' tag helps to build a table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id` int(11) NOT NULL,
  `catagory_id` tinyint(4) NOT NULL,
  `topic_title` varchar(150) NOT NULL,
  `topic_creator` int(11) NOT NULL,
  `topic_last_user` int(11) NOT NULL,
  `topic_date` datetime NOT NULL,
  `topic_reply_date` datetime NOT NULL,
  `topic_views` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`id`, `catagory_id`, `topic_title`, `topic_creator`, `topic_last_user`, `topic_date`, `topic_reply_date`, `topic_views`) VALUES
(11, 2, 'Test Topic', 0, 5, '2017-07-27 01:07:18', '2017-07-27 14:32:57', 16),
(12, 1, 'Test Topic 1', 0, 0, '2017-07-27 02:27:35', '2017-07-27 02:32:18', 26);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(128) NOT NULL,
  `lastname` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `pwd` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `username`, `pwd`) VALUES
(1, 'Alazar', 'Kassahun', 'alazar.kas@gmail.com', 'laz123', '$2y$10$H0mcq65e/jWW/GjtaBIUBuEnkHvoNvKOrfGnXUwlwi7OpafXf69sO'),
(2, 'Natnael', 'Kassahun', 'nati.kas@gmailcom', 'nati123', '$2y$10$xUT8Go3z/DT5uJ4mAeCt1OH1JGZ9724d0/BKHuAp97nE3M8whN1Za'),
(4, 'Habtamu', 'Alem', 'habtish212@gmail.com', 'habtish212', '$2y$10$D0r09EvBFQPvnqF2X5CgjuoZrV3kxLqS3BsADuWRileC8thv3WnM6'),
(5, 'Natnael', 'Demelash', 'natid@gmail.com', 'nati124', '$2y$10$7btS1crWbpe4UWkyjtJr3eS.RMg2VjmzFrFUevE9QtJmQ0EFb4HXG'),
(6, 'betse', 'bet', 'bets@gmail.com', 'bet', '$2y$10$ZGKsim7JZFknu9nH5zWx1.ho5OdCJDOwHTEIZaa5MBj7q/LW/YXfa'),
(7, 'qwerty', 'qwer', 'bet@gmail.com', 'qwe', '$2y$10$0u6V27wtrRY0xeHFdSN7LejnxttOWv3akUikf1iSaFuHjzUwVxbt6'),
(8, 'qwerty', 'qwer', 'bet@gmail.com', 'qwerft', '$2y$10$3mQJ.1Z3uQd8fjxr8VLJyeJhLqwwAcUoWWeeOdXuLtOe.Bk73Wwsy'),
(9, 'qwerty', 'qwer', 'bet@gmail.com', 'qazx', '$2y$10$qx/5yi5DMJz5fTDZdhQ7AufY02AtchvpoOYsueZMxoLHfpQ4RCbQW'),
(10, 'asa', 'asas', 'abebe@gmail.com', 'asdf', '$2y$10$GvjaLJm5OSWhfPHx1LXZg.xgskdcu2ZDlOT8UQlmYo0G3scClrz5m'),
(11, 'zxxz', 'zxzx', 'zxz@gmail.com', 'zxcczx', '$2y$10$qn3BYIs3jLIK572hIFpY/OsnPoGDlmrsVghH.SifB8DdWZHbZSFrG'),
(12, 'zxxz', 'zxzx', 'zxz@gmail.com', 'lkjh', '$2y$10$fdd.D/ixBp1wzERVwZTZ1e3cfX58qDDFOvCRm.dMrSannoea9n0oC'),
(13, 'terte', 'tetrert', 'tetet@gmail.com', 'wyhj', '$2y$10$.ujezPF6TQl6bk5FpT4eruOgwFV1OLb4qZCgAEKSCS4/zg5oN5Mi2'),
(14, 'oiuyt', 'iuytr', 'vgt@gmail.com', 'nhg', '$2y$10$9uUnyCiq.rfqN9fr/vYeROUJOhDWO5gSrwFylABJplxWacH6Q49dS'),
(15, 'tert', 'erter', 'bft12@gmail.com', 'oiuyt', '$2y$10$7AVr2os1tCXp6kVVmcCwme1ButwrbiheiMAkx9xOQGHHwsLqxnnjW'),
(16, 'rwe', 'rerqe', 'qwe@gmaim.com', 'kjh', '$2y$10$XAGGZ0/4n.w3vGb5I3awqOCw.xLt8dMloFTwlnLY3oM6NKYj1P58O'),
(17, 'wqe', 'qwe', 'elnatal@gmail.com', 'ytrdc', '$2y$10$zyj4L.vMlITAzWknSwwdPuKq47gCAoePbIxx/YWWrKFzbK/5pyLYa'),
(18, 'qwerwq', 'wrqwrqwe', 'vgt@gmail.com', 'gfd123', '$2y$10$MvFjqujpolV.ncMSQ0.n3eTBIrTV9m5xk5fAUC5VUkTdZHYE5y2mO'),
(19, 'qe', 'qw', 'qwe@gmaim.com', '1wde', '$2y$10$7ePEYC3S0JauXn/Z7XxYPO1tnDvbizyP7gdMKFB1ulug5fdtOoKtS'),
(20, 'qwerty', 'qewwqe', 'qwe@gmaim.com', '12trew', '$2y$10$5S2nWaSYitxj0hmbSl/OZOJ72AuVLbOJ1PjGsw8Ap1NXFWPvPHmva'),
(21, 'ghhg', 'jhgjhj', 'qwe@gmaim.com', 'lkjkl', '$2y$10$1CahsKcO9Limv9wZgLJqwuUSSp7F35dyNii527bnSLV3lbnvIdsry'),
(22, 'eqw', 'eqw', 'qwe@gmaim.com', 'mike@gmail.com', '$2y$10$6HOuzswcUep1IvGNF03.cu.OWhq.hTmdrn6AURekD/RlImLo/0T9S'),
(23, 'hgfd', 'hbdf', 'qwe@gmaim.com', 'betsegaws@gmail.com', '$2y$10$0e.X3dbJ89bQfhkOwWTnXuaOUE9DITZhuhmI4T8pcv5Lg9olTzBd.'),
(24, 'qwr', 'rqwr', 'qwe@gmaim.com', 'ghdsdgf', '$2y$10$czbkdQcEErl3fR7VC7cku.rHkhS0Nkn8GC2HMHjn5nw9O4DpdQzC.'),
(25, 'rw', 'wq', 'qwe@gmaim.com', 'jhgt43', '$2y$10$2056Nb9AFbq872cT5JriUOp9IPnQmbPQQc/FxlXZvLHhZs19h8IxC'),
(26, 'ghhg', 'qweq', 'qwe@gmaim.com', 'gefw123', '$2y$10$VOkY.Yynbg40cgFoZIXK3O9/FzduW.2fnZZS9rMhMIblcT4M./pAK');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `catagories`
--
ALTER TABLE `catagories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`chid`),
  ADD KEY `courseid` (`courseid`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`courseid`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`optid`),
  ADD KEY `qid` (`qid`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`qid`),
  ADD KEY `courseid` (`courseid`),
  ADD KEY `chid` (`chid`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `catagories`
--
ALTER TABLE `catagories`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `chid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `courseid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `optid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `qid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `chapters`
--
ALTER TABLE `chapters`
  ADD CONSTRAINT `chapters_ibfk_1` FOREIGN KEY (`courseid`) REFERENCES `course` (`courseid`);

--
-- Constraints for table `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_ibfk_1` FOREIGN KEY (`qid`) REFERENCES `question` (`qid`);

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`courseid`) REFERENCES `course` (`courseid`),
  ADD CONSTRAINT `question_ibfk_2` FOREIGN KEY (`chid`) REFERENCES `chapters` (`chid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
