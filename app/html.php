<?php
  session_start();
  if (!isset($_SESSION['id'])) {
    header("Location: index.php");
  }

  include('header.html');
?>

<!--========== column ==========-->
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<img class="html_img img-responsive" src="img/html.png">	
		</div>	
		<div class="col-md-6">
			<div class="text_html" id="text_html">
				<h2 id="text_header">Hypertext Markup Language</h2>
				<p id="text_body">
					Hypertext Markup Language (HTML) is the standard markup language for creating web pages and web applications. With Cascading Style Sheets (CSS) and JavaScript it forms a triad of cornerstone technologies for the World Wide Web. Web browsers receive HTML documents from a webserver or from local storage and render them into multimedia web pages. HTML describes the structure of a web page semantically and originally included cues for the appearance of the document.
				</p>
				<img class="img-responsive" src="img/css.PNG">
			</div>
		</div>	
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="item_btn col-md-6">
			<button onclick="location.href='tutorh.php'" type="button" class="btn btn-default active btn-lg">Start Learning</button>
		</div>
		<div class="item_btn col-md-6">
			<button onclick="location.href='htmltests.php'" type="button" class="btn btn-default active btn-lg">Test Yourself</button>
		</div>
	</div>
</div>
<!--========== column ==========-->
        <?php
 include('footer.html');
?>
