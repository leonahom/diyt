<?php
include 'dbh.php';
class User
{
    function editpro()
    {
        $str = "";

        if (isset($_POST['update-btn'])) {
            global $conn;
            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];
            $email = $_POST['email'];
            $oldPwd = $_POST['oldPwd'];
            $newPwd = $_POST['newPwd'];
            $confirmPwd = $_POST['confirmPwd'];
            $username = $_SESSION['id'];
            $sql = "SELECT * FROM users WHERE id = '$username' LIMIT 1";
            $result = $conn->query($sql);
            if (!$result) {
                header("Location: index.php?problem1");
            } else {
                $row = $result->fetch_assoc();
                $hash_pwd = $row['pwd'];
                $hash = password_verify($oldPwd, $hash_pwd);
                $sql = "SELECT * FROM users WHERE id = '$username' AND pwd = '$hash_pwd' LIMIT 1";
                $result = $conn->query($sql);
                if (!$result) {
                    header("Location: index.php?problem2");
                } else {
                    if ($row = $result->fetch_assoc()) {
                        $id = $row['id'];
                        $str = $str . $id;
                        $username = $_POST['username'];
                        if ($newPwd = $confirmPwd) {
                            $encryptedPwd = password_hash($newPwd, PASSWORD_DEFAULT);
                            $sql = "UPDATE users SET firstname = '$firstname', lastname = '$lastname' ,email= '$email' ,username= '$username' ,pwd= '$encryptedPwd' WHERE id= '$id' LIMIT 1 ";
                            $result = $conn->query($sql);
                            if (!$result) {
                                header("Location: home.php?problem3");
                            }

                            if ($result) {
                                $_SESSION['confirm'] = 'Account updated successfully';
                                header("Location: profile.php?update=success");
                                exit();
                            } else {
                                $str = "update query failed";
                            }

                        }


                    }
                }
            }
        }
        return $str;
    }


    function checkuser($p)
    {
        global $conn;
        $sql = "SELECT * FROM users where username='$p'";
        $result = mysqli_query($conn, $sql);

        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {
                return "This username already taken";
            }
        } else {
            return "username available";
        }
        mysqli_close($conn);
    }

    function registerUser($firstName, $lastName, $email, $username, $pwd, $cpwd)
    {
        $sql = "SELECT * FROM users WHERE username = '$username'";
        global $conn;
        $result = $conn->query($sql);
        $usernameCheck = mysqli_num_rows($result);
        if ($usernameCheck > 0) {
            header("Location: signup.php?error=usernametaken");
            exit();
        }
        if ($pwd != $cpwd) {
            header("Location: signup.php?error=password");
            exit();
        } else {
            $encryptedPwd = password_hash($pwd, PASSWORD_DEFAULT);
            $sql = "INSERT INTO users (firstname, lastname, email, username, pwd) VALUES ('$firstName', '$lastName', '$email', '$username', '$encryptedPwd')";

            $result = $conn->query($sql);


            $sql2 = "SELECT * FROM users WHERE username = '$username'";
            $result2 = mysqli_query($conn, $sql2);

            if (mysqli_num_rows($result2) > 0) {
                // output data of each row
                while ($row2 = mysqli_fetch_assoc($result2)) {
                    $userid = $row2['id'];
                }
            } else {
                echo "0 results";
            }


            $sql3 = "INSERT INTO progress ( userid, html, css, java,php,python ) VALUES ('$userid', '0', '0', '0', '0','0')";
            $result3 = $conn->query($sql3);


            if ($result) {
                $_SESSION['confirm'] = 'Account created successfully';
                header("Location: ../index.php?signup=success");
                exit();
            } else {
                echo "update query failed";
            }

        }
    }



    function TRegisterUser($firstName, $lastName, $email, $username, $pwd, $cpwd)
    {
        $sql = "SELECT * FROM users WHERE username = '$username'";
        global $conn;
        $result = $conn->query($sql);
        $usernameCheck = mysqli_num_rows($result);
        if ($usernameCheck > 0) {
            header("Location: signup.php?error=usernametaken");
            exit();
        }
        if ($pwd != $cpwd) {
            header("Location: signup.php?error=password");
            exit();
        } else {
            $encryptedPwd = password_hash($pwd, PASSWORD_DEFAULT);
            $sql = "INSERT INTO users (firstname, lastname, email, username, pwd) VALUES ('$firstName', '$lastName', '$email', '$username', '$encryptedPwd')";

            $result = $conn->query($sql);


            $sql2 = "SELECT * FROM users WHERE username = '$username'";
            $result2 = mysqli_query($conn, $sql2);

            if (mysqli_num_rows($result2) > 0) {
                // output data of each row
                while ($row2 = mysqli_fetch_assoc($result2)) {
                    $userid = $row2['id'];
                }
            } else {
                echo "0 results";
            }


            $sql3 = "INSERT INTO progress ( userid, html, css, java,php,python ) VALUES ('$userid', '0', '0', '0', '0','0')";
            $result3 = $conn->query($sql3);


            if ($result) {
                $_SESSION['confirm'] = 'Account created successfully';
                return true;
                exit();
            } else {
                return false;
                echo "update query failed";
            }

        }
    }


    function login($username, $pwd)
    {
        $sql = "SELECT * FROM users WHERE username = '$username'";
        global $conn;
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();
        $hashPwd = $row['pwd'];
        $hash = password_verify($pwd, $hashPwd);

        if ($hash == 0) {
            echo "<script>alert('incorrect username/passaword combination');location='../index.php';</script>";
            return false;

        } else {
            $sql = "SELECT * FROM users WHERE username = '$username' AND pwd = '$hashPwd'";
            $result = $conn->query($sql);
            if (!$row == $result->fetch_assoc()) {
                echo "Your username or password is incorrect!!";
                return false;
            } else {
                $_SESSION['id'] = $row ['id'];

            }
            header("Location: ../home.php");
            return true;
        }
    }
    function TLogin($username, $pwd)
    {
        $sql = "SELECT * FROM users WHERE username = '$username'";
        global $conn;
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();
        $hashPwd = $row['pwd'];
        $hash = password_verify($pwd, $hashPwd);

        if(strlen($pwd)<8){
            return false;
        }

        if ($hash == 0) {
            echo "<script>alert('incorrect username/passaword combination');location='../index.php';</script>";
            return false;

        } else {
            $sql = "SELECT * FROM users WHERE username = '$username' AND pwd = '$hashPwd'";
            $result = $conn->query($sql);
            if (!$row == $result->fetch_assoc()) {
                echo "Your username or password is incorrect!!";
                return false;
            } else {
                $_SESSION['id'] = $row ['id'];

            }
            return true;
        }
    }


}
$user= new User();
?>