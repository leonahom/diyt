create table testtaken(
	id int auto_increament,
	userid int,
	chid int,
	result int,
	primary key(id),
	foreign key(userid) references users(id),
	foreign key(chid) references chapters(chid) 
);
create table progress(
id int auto_increment,
userid int,
html int,
css int,
java int,
php int,
python int,
primary key(id),
foreign key(userid) references users(id)
);