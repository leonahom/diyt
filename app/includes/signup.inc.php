<?php 
session_start();
include 'dbh.php';
include '../models/user_model.php';
$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$email = $_POST['email'];
$username = $_POST['userName'];
$pwd = $_POST['pwd'];
$cpwd = $_POST['cpwd'];

if (empty($firstName)) {
	header("Location: ../index.php?error=empty");
	exit();
}
if (empty($lastName)) {

	header("Location: ../index.php?error=empty");
	exit();
}
if (empty($email)) {
	header("Location: ../index.php?error=empty");
	exit();
}
if (empty($username)) {
	header("Location: ../index.php?error=empty");
	exit();
}
if (empty($pwd)) {
	header("Location: ../index.php?error=empty");
	exit();
}
if (empty($cpwd)) {
	header("Location: ../index.php?error=empty");
	exit();
}
else{
	$res=$user->registerUser($firstName,$lastName,$email,$username,$pwd,$cpwd);
}
