<?php 
session_start();
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Sign Up</title>
</head>
<body>
	<!--<form action="includes/signup.inc.php" method="POST">
		<label for="firstName">First Name:</label>
		<br>
		<input type="text" name="firstName" id="firstName" placeholder="Enter Your First Name">
		<br>
		<label for="lastName">Last Name:</label>
		<br>
		<input type="text" name="lastName" id="lastName" placeholder="Enter Your Last Name">
		<br>
		<label for="email">Email:</label>
		<br>
		<input type="text" name="email" id="email" placeholder="Enter Your Email Address">
		<br>
		<label for="userName">Username:</label>
		<br>
		<input type="text" name="userName" id="userName" placeholder="Enter Your Username">
		<br>
		<label for="pwd">Password:</label>
		<br>
		<input type="password" name="pwd" id="pwd" placeholder="Enter Your Password">
		<br>
		<label for="cpwd">Confirm Password:</label>
		<br>
		<input type="password" name="cpwd" id="cpwd" placeholder="Confirm Your Password">
		<br>
		<button type="submit">Sign Up</button>
	</form>-->
	<form action="includes/login.inc.php" method="POST">
		<label for="userName">Username:</label>
		<br>
		<input type="text" name="userName" id="userName" placeholder="Enter Your Username">
		<br>
		<label for="pwd">Password:</label>
		<br>
		<input type="password" name="pwd" id="pwd" placeholder="Enter Your Password">
		<br>
		<button type="submit">Sign In</button>
	</form>
	<form action="includes/logout.inc.php">
		<button type="submit">Sign Out</button>
	</form>
	<?php 
        if (isset($_SESSION['id'])) {
          echo "You are signed in!!";
        }
        else{
          echo "You are not signed in!!";
        }
       ?>
</body>
</html>