<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
    <title>home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet" type="text/css">
    <link href="vendor/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="css/animate.css" rel="stylesheet">
    <link href="vendor/swiper/css/swiper.min.css" rel="stylesheet" type="text/css"/>

    <!-- THEME STYLES -->
    <link href="css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="css/layout_anima.css">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="css/fontmfizz/font-mfizz.css">
    <link href="fonts/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">


    <link rel="stylesheet" href="css\bootstrap.css">

    <script type="text/javascript">
        function showUser(str) {

            if (str == "") {
                document.getElementById("txtHint").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("txtHint").innerHTML = this.responseText;
                }
            }
            xmlhttp.open("GET", "checkuser.php?p=" + str, true);
            xmlhttp.send();
        }

        // body...

    </script>
    <style type="text/css">

        .navbar {
            border-bottom: 1px solid #337ab7;

        }

        .navbar-container {
            background-color: white;
        }

        .text-center {
            padding-left: 0;
            padding-top: 4%;
        }

        nav {
            overflow: hidden;
        }

        #hu {
            padding-top: 4%;
        }

        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.8);
            padding-top: 60px;
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
            border: 1px solid #888;
            width: 30%; /* Could be more or less, depending on screen size */

            padding: 2%;
        }

        .close-btn {
            position: absolute;
            top: 20px;
            right: 55px;
            color: white;
            font-weight: bold;
            font-size: 40px;
        }

        .close-btn:hover,
        .closebtn:focus {
            color: red;
            cursor: pointer;
        }

        .clearfix::after {
            content: "";
            clear: both;
            display: table;
        }

        .big_img {
            background-image: url("img/logo1.png");
            background-size: 50% 50%;
            margin-top: 220px;
            padding-top: 40%;
            background-repeat: no-repeat;
            margin-left: 680px;
        }

        .cont_nav {
            height: 84px;
        }

        .nav {
            margin-top: 1%;
        }

    </style>
</head>
<body>
<nav class="navbar navbar-container navbar-fixed-top">
    <div class="cont_nav container-fluid">
        <ul class="nav navbar-nav navbar-right">
            <li>
                <div id="confirm1" style="margin-top: 17px; margin-right:30px;"></div>
            </li>
            <li><a href="#" id="signup" onclick="document.getElementById('modal1').style.display='block'"><span
                            class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li id="login"><a href="#" onclick="document.getElementById('modal2').style.display='block'"><span
                            class="glyphicon glyphicon-log-in"></span> Login</a></li>
        </ul>

        <div class="modal" id="modal1">
            <span id="close" class="close-btn" onclick="document.getElementById('modal1').style.display='none'">X</span>

            <form method="POST" class="modal-content" name="signup" onsubmit="return validatesignup();"
                  action="includes/signup.inc.php">
                <div class="row">
                    <div class="col-xs-6 col-md-6">
                        <label>First Name:</label><br><br>
                        <input class="form-control" name="firstName" placeholder="First Name" type="text" autofocus/>
                    </div>
                    <div class="col-xs-6 col-md-6">
                        <label>Last Name:</label><br><br>
                        <input class="form-control" name="lastName" placeholder="Last Name" type="text"/>
                    </div>
                </div>
                <br>
                <label>Email:</label><br><br>
                <input class="form-control" name="email" placeholder="Your Email" type="email"/><br>
                <label>Username:</label><br><br>
                <input class="form-control" name="userName" id="username" onkeyup="showUser(this.value)"
                       placeholder="Username" type="text"/><br>
                <div style="color: red;" id="txtHint"></div>
                <label>Password</label><br><br>
                <input class="form-control" name="pwd" id="password" placeholder="New Password" type="password"/><br>
                <label>Confirm Password:</label><br><br>
                <input class="form-control" name="cpwd" placeholder="Confirm Password" type="password"/>
                <br/>
                <br/>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
            </form>


        </div>
        <div class="modal" id="modal2">
            <span id="close" class="close-btn" onclick="document.getElementById('modal2').style.display='none'">X</span>


            <form action="includes/login.inc.php" method="POST" class="modal-content" name="login"
                  onsubmit="return validatelogin();">
                <div class="row">
                    <label>Username:</label><br><br>
                    <input class="form-control" id="logusername" name="userName" placeholder="Username"
                           type="text"/><br>
                    <label>Password:</label><br><br>
                    <input class="form-control" id="logpassword" name="pwd" placeholder="Password" type="password"/><br>

                    <br/>
                    <br/>
                    <button id="loginbtn" class="btn btn-lg btn-primary btn-block" type="submit">Sign In</button>
                </div>
            </form>


        </div>
    </div>
</nav>
<div class="big_img">

</div>
<?php

if (isset($_SESSION['confirm'])): ?>
    <script>
        document.getElementById('confirm1').innerHTML = '<?php echo $_SESSION['confirm'];?>';
    </script>
    <?php unset($_SESSION['confirm']); endif; ?>
<script>

    function validatesignup() {
        var first = document.forms["signup"]["firstName"].value;
        var last = document.forms["signup"]["lastName"].value;
        var mail = document.forms["signup"]["email"].value;
        var user = document.forms["signup"]["userName"].value;
        var password = document.forms["signup"]["pwd"].value;
        var conpassword = document.forms["signup"]["cpwd"].value;
        var x = 0

        if (first == "" || first == null) {
            document.forms["signup"]["firstName"].style.borderColor = "red";
            document.forms["signup"]["firstName"].placeholder = "Cannot be left Empty";
            x = 1;
        }

        if (last == "" || last == null) {
            document.forms["signup"]["lastName"].style.borderColor = "red";
            document.forms["signup"]["lastName"].placeholder = "Cannot be left Empty";
            x = 1;
        }
        if (user == "" || user == null) {
            document.forms["signup"]["userName"].style.borderColor = "red";
            document.forms["signup"]["userName"].placeholder = "Cannot be left Empty";
            x = 1;
        }
        if (document.getElementById('txtHint').innerHTML == 'This username already taken') {
            x = 1;
        }
        if (mail == "" || mail == null) {
            document.forms["signup"]["email"].style.borderColor = "red";
            document.forms["signup"]["email"].placeholder = "Cannot be left Empty";
            x = 1;
        }
        if (password == "" || password == null) {
            document.forms["signup"]["pwd"].style.borderColor = "red";
            document.forms["signup"]["pwd"].placeholder = "Cannot be left Empty";
            x = 1;
        }
        if (conpassword == "" || conpassword == null) {
            document.forms["signup"]["cpwd"].style.borderColor = "red";
            document.forms["signup"]["cpwd"].placeholder = "Cannot be left Empty";
            x = 1;
        }
        if (password.length > 1 && password.length < 8) {
            document.forms["signup"]["pwd"].style.borderColor = "red";
            document.forms["signup"]["pwd"].value = "";
            document.forms["signup"]["pwd"].placeholder = "Password has to be at least 8 characters";
            x = 1;
        }
        if ((password != null || password != "") && (conpassword == null || conpassword == "")) {
            document.forms["signup"]["cpwd"].placeholder = "Cannot be left Empty";
        }
        else if (password != conpassword) {
            document.forms["signup"]["pwd"].style.borderColor = "red";
            document.forms["signup"]["cpwd"].style.borderColor = "red";
            document.forms["signup"]["pwd"].value = "";
            document.forms["signup"]["cpwd"].value = "";
            document.forms["signup"]["cpwd"].placeholder = "Password does not match"
            x = 1;
        }
        if (x == 1) {
            return false;
        }

    }

    function validatelogin() {
        var use = document.forms["login"]["userName"].value;
        var pass = document.forms["login"]["pwd"].value;
        var x = 0

        if (use == "" || use == null) {
            document.forms["login"]["userName"].style.borderColor = "red";
            document.forms["login"]["userName"].placeholder = "Cannot be left Empty";
            x = 1;
        }
        if (pass == "" || pass == null) {
            document.forms["login"]["pwd"].style.borderColor = "red";
            document.forms["login"]["pwd"].placeholder = "Cannot be left Empty";
            x = 1;
        }
        if (x == 1) {
            return false;
        }
    }
</script>
<?php
include('footer.html');
?>
