<?php 
session_start();
include 'dbh.php';

$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$email = $_POST['email'];
$username = $_POST['userName'];
$pwd = $_POST['pwd'];
$cpwd = $_POST['cpwd'];

if (empty($firstName)) {
	header("Location: ../index.php?error=empty");
	exit();
}
if (empty($lastName)) {
	header("Location: ../index.php?error=empty");
	exit();
}
if (empty($email)) {
	header("Location: ../index.php?error=empty");
	exit();
}
if (empty($username)) {
	header("Location: ../index.php?error=empty");
	exit();
}
if (empty($pwd)) {
	header("Location: ../index.php?error=empty");
	exit();
}
if (empty($cpwd)) {
	header("Location: ../index.php?error=empty");
	exit();
}
else{
	$sql = "SELECT * FROM users WHERE username = '$username'";
	$result = $conn->query($sql);
	$usernameCheck = mysqli_num_rows($result);
	if ($usernameCheck > 0) {
		header("Location: signup.php?error=usernametaken");
		exit();
	}
	if ($pwd != $cpwd) {
		header("Location: signup.php?error=password");
		exit();
	}
	else{
		$encryptedPwd = password_hash($pwd, PASSWORD_DEFAULT);
		$sql = "INSERT INTO users (firstname, lastname, email, username, pwd) VALUES ('$firstName', '$lastName', '$email', '$username', '$encryptedPwd')";
		$result = $conn->query($sql);
		header("Location: ../../view/index.php");
	}
}