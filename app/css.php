<?php
session_start();
  if (!isset($_SESSION['id'])) {
    header("Location: index.php");
  }

  include('header.html');
?>

<!--========== column ==========-->
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<img class="css_img img-responsive" src="img/css1.png" width="420px">	
		</div>	
		<div class="col-md-6">
			<div class="text_html" id="text_html">
				<h2 id="text_header">Cascading Style Sheets</h2>
				<p id="text_body">
					Cascading Style Sheets (CSS) is a style sheet language used for describing the presentation of a document written in a markup language.CSS is designed primarily to enable the separation of presentation and content, including aspects such as the layout, colors, and fonts. This separation can improve content accessibility, provide more flexibility and control in the specification of presentation characteristics, enable multiple HTML pages to share formatting by specifying the relevant CSS in a separate .css file, and reduce complexity and repetition in the structural content.
				</p>
				<img class="img-responsive" src="img/css-declaration.png">
			</div>
		</div>	
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="item_btn col-md-6">
			<button onclick="location.href='tutorc.php'" type="button" class="btn btn-default active btn-lg">Start Learning</button>
		</div>
		<div class="item_btn col-md-6">
			<button onclick="location.href='csstests.php'" type="button" class="btn btn-default active btn-lg">Test Yourself</button>
		</div>
	</div>
</div>
<!--========== column ==========-->
        <?php
 include('footer.html');
?>
