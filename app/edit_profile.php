<?php
session_start();
  if (!isset($_SESSION['id'])) {
    header("Location: index.php");
  }
  include('header.html');
  include 'includes/dbh.php';
  ?>

<div id="page-wrapper">
        <div class="row" style="margin-top: 150px;">
            <div class="col-lg-12">
                <h1 class="page-title" style="text-align: center;">Update Profile</h1>

            </div>
            <!-- /.col-lg-12 -->


        </div>

            <!-- /.panel-heading -->
            <div class="panel-body col-md-4 col-md-offset-4">
        

            <?php  
                    if (isset($_SESSION['id'])) {
                        $ID = $_SESSION['id'];
            
	                    $sql = "SELECT * FROM users WHERE ID ='$ID'"; 
	                    $result = $conn->query($sql);
	                    $row = mysqli_fetch_array($result);
	                    $result = $conn->query($sql);
	                    $row = $result->fetch_array(MYSQLI_BOTH);
                   }

                 ?>
       <form method="POST" action="editProfile.php" name="edit" onsubmit="return editprofile();" >
        <div class="row">
          <div class="form-group col-md-6">
          <label for='old-pwd'>First Name:</label>
            <input class="form-control" placeholder="First name" name="firstname" type="text" value="<?php  if (isset($ID)){ echo($row['firstname']); }?>">
          </div>
          <div class="form-group col-md-6">
          <label for='old-pwd'>Last Name:</label>
            <input class="form-control" placeholder="Last name" name="lastname" type="text" value="<?php if (isset($ID)) { echo($row['lastname']); }?>">
          </div>
        </div>
        <div class="form-group">
        <label for='old-pwd'>Username:</label>
          <input class="form-control" placeholder="Username" name="username" type="text" value="<?php  if (isset($ID)) { echo($row['username']);} ?>">
        </div>
        <div class="form-group">
        <label for='old-pwd'>Email:</label>
          <input style="margin-top: 3px;" class="form-control" id="email" placeholder="E-mail" name="email" type="email" value="<?php  if (isset($ID)) {echo($row['email']); }?>">
        </div>
        <div class="form-group">
        <label for='old-pwd'>Old Password:</label>
          <input class="form-control" id="old-pwd" name="oldPwd" type="password">
        </div>
         <div class="form-group">
         <label for='old-pwd'>New Password:</label>
          <input class="form-control" id="new-pwd" name="newPwd" type="password">
        </div>
        <div class="form-group">
        <label for='old-pwd'>Confirm-new Password:</label>
          <input class="form-control" id="con-old-pwd" name="confirmPwd" type="password">
        </div>
        <div class="row">
        	<div class="col-md-6">
        		<a class="btn btn-lg btn-success btn-block"    data-toggle="modal" data-target="#confirm">Update</a>
        	</div>
        	<div class="col-md-6">
        		<a class="btn btn-lg btn-danger btn-block" data-toggle="modal" data-target="#confirm">cancel</a>
        	</div>
        </div> 
         
       <div id="confirm" class="modal fade" role="dialog">
          <div class="modal-dialog">
          <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-body">
                <p>Do you really want to update this profile?</p>
                <p>This might affect your account!</p>
              </div>
              <div class="modal-footer">
                <button type="submit" name="update-btn" class="btn btn-lg btn-success " >Update</button>
                <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </div>
        </div>              
        <!-- Change this to a button or input when using this as a form -->                               
      </form>
   
    <!-- <div class="alert alert-success">
      <strong>Successful!</strong> User information successfully updated.
    </div> -->
    
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
 <script>
      function editprofile() {
 
        var edfirst = document.forms["edit"]["firstname"].value;
        var edlast = document.forms["edit"]["lastname"].value;
        var eduser = document.forms["edit"]["username"].value;
        var edmail = document.forms["edit"]["email"].value;
        var oldpassword = document.forms["edit"]["oldPwd"].value;
        var edpassword = document.forms["edit"]["newPwd"].value;
        var edconpassword = document.forms["edit"]["confirmPwd"].value;
        var x = 0

        if (edfirst == "" || edfirst == null) {
          document.forms["edit"]["firstname"].style.borderColor = "red";
          document.forms["edit"]["firstname"].placeholder = "Cannot be left Empty";
          x = 1;
          } 
        if (edlast == "" || edlast == null) {
          document.forms["edit"]["lastname"].style.borderColor = "red";
          document.forms["edit"]["lastname"].placeholder = "Cannot be left Empty";
          x = 1;
          }
       
        if(eduser == "" || eduser == null) {
          document.forms["edit"]["username"].style.borderColor = "red";
          document.forms["edit"]["username"].placeholder = "Cannot be left Empty";
          x = 1;
          }
        if(edmail == "" || edmail == null) {
          document.forms["edit"]["email"].style.borderColor = "red";
          document.forms["edit"]["email"].placeholder = "Cannot be left Empty";
          x = 1;
          }
        if(oldpassword == "" || oldpassword == null) {
          document.forms["edit"]["oldPwd"].style.borderColor = "red";
          document.forms["edit"]["oldPwd"].placeholder = "Cannot be left Empty";
          x = 1;
          }
        if(edpassword == "" || edpassword == null) {
          document.forms["edit"]["newPwd"].style.borderColor = "red";
          document.forms["edit"]["newPwd"].placeholder = "Cannot be left Empty";
          x = 1;
          }
        if(edconpassword == "" || edconpassword == null) {
          document.forms["edit"]["confirmPwd"].style.borderColor = "red";
          document.forms["edit"]["confirmPwd"].placeholder = "Cannot be left Empty";
          x = 1;
          }
        if (edpassword.length > 1 && edpassword.length < 8) {
          document.forms["edit"]["newPwd"].style.borderColor = "red";
          document.forms["edit"]["newPwd"].value = "";
          document.forms["edit"]["newPwd"].placeholder = "Password has to be at least 8 characters";
          x = 1;
          }
        if ((edpassword != null || edpassword != "") && (edconpassword == null || edconpassword == "")) {
          document.forms["edit"]["confirmPwd"].placeholder = "Cannot be left Empty";
          x = 1;
          }
        else if (edpassword != edconpassword) {
          document.forms["edit"]["newPwd"].style.borderColor = "red";
          document.forms["edit"]["confirmPwd"].style.borderColor = "red";
          document.forms["edit"]["newPwd"].value = "";
          document.forms["edit"]["confirmPwd"].value = "";
          document.forms["edit"]["confirmPwd"].placeholder = "Password does not match";
          x = 1;
          }
        if (x == 1) {
          
return false;
        }
        
      }
 </script>


  <?php
   include('footer.html'); 
  ?>